﻿using NUnit.Framework;
using System.Collections.Generic;

namespace csharp
{
    [TestFixture]
    public class GildedRoseTest
    {
        [Test]
        public void QualityOfNormalItemsAfterUpdate()
        {
            IList<CategorizedItem> Items = new List<CategorizedItem> { new NormalItem { Name = "Normal item", SellIn = 10, Quality = 10 } };
            int decreaseValue = ((NormalItem)Items[0]).DecreaseQualityValue;
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(9, Items[0].SellIn);
            Assert.AreEqual(10 - decreaseValue, Items[0].Quality);
        }

        [Test]
        public void QualityOfAgedItemsAfterUpdate()
        {
            IList<CategorizedItem> Items = new List<CategorizedItem> { new AgedItem { Name = "Aged item", SellIn = 10, Quality = 10 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(9, Items[0].SellIn);
            Assert.AreEqual(11, Items[0].Quality);
        }

        [Test]
        public void QualityOfLegendaryItemsAfterUpdate()
        {
            IList<CategorizedItem> Items = new List<CategorizedItem> { new LegendaryItem { Name = "Legendary item", SellIn = 0, Quality = 80 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(0, Items[0].SellIn);
            Assert.AreEqual(80, Items[0].Quality);
        }

        [Test]
        public void QualityOfConjuredItemsAfterUpdate()
        {
            IList<CategorizedItem> Items = new List<CategorizedItem> { new ConjuredItem { Name = "Conjured item", SellIn = 10, Quality = 10 } };
            int decreaseValue = 2 * ((NormalItem)Items[0]).DecreaseQualityValue;
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(9, Items[0].SellIn);
            Assert.AreEqual(10 - decreaseValue, Items[0].Quality);
        }

        [Test]
        public void QualityOfUnusualItemsAfterUpdate()
        {
            UnusualItem uItem = new UnusualItem { Name = "Unusual item", SellIn = 10, Quality = 10 };
            uItem.QualityEvaluator = () => uItem.Quality -= 1;

            IList<CategorizedItem> Items = new List<CategorizedItem> { uItem };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(9, Items[0].SellIn);
            Assert.AreEqual(9, Items[0].Quality);
        }

        [Test]
        public void ItemQualityIsInBound()
        {
            IList<CategorizedItem> Items = new List<CategorizedItem> { new NormalItem { Name = "Item", SellIn = 10, Quality = 10 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(true, Items[0].IsInBound());
        }

        [Test]
        public void ItemQualityIsNotInBound()
        {
            IList<CategorizedItem> Items = new List<CategorizedItem> { new NormalItem { Name = "Item", SellIn = 0, Quality = -1 } };
            GildedRose app = new GildedRose(Items);
            app.UpdateQuality();
            Assert.AreEqual(false, Items[0].IsInBound());

            Items[0].Quality = 60;
            Assert.AreEqual(false, Items[0].IsInBound());
        }
    }
}
