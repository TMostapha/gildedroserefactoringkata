﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public class ConjuredItem : NormalItem
    {
        public override void UpdateQuality()
        {
            this.Quality -= 2 * this.DecreaseQualityValue;
            UpdateSellIn();
        }
    }
}
