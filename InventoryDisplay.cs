﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public class InventoryDisplay
    {
        public readonly GildedRose App;

        public InventoryDisplay(GildedRose app)
        {
            this.App = app;
        }
        public void DisplayOverDays()
        {
            Console.WriteLine("OMGHAI!");

            int days = 31;
            for (var i = 0; i < days; i++)
            {
                Console.WriteLine("-------- day " + i + " --------");
                Console.WriteLine("name, sellIn, quality");
                foreach(var item in this.App.Items)
                {
                    System.Console.WriteLine(item);
                }
                Console.WriteLine("");
                App.UpdateQuality();
            }

            Console.ReadKey();
        }
    }
}
