﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public class UnusualItem : CategorizedItem
    {
        public Action QualityEvaluator { get; set; }
        public override void UpdateQuality()
        {
            QualityEvaluator();
            UpdateSellIn();
        }
    }
}
