﻿using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        public IList<CategorizedItem> Items;
        public GildedRose(IList<CategorizedItem> Items)
        {
            this.Items = Items;
        }

        public void UpdateQuality()
        {
            foreach(var item in Items)
            {
                item.UpdateQuality();
            }
        }
    }
}
