﻿using System;
using System.Collections.Generic;

namespace csharp
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var builder = new InventoryBuilder();

            var app = new GildedRose(builder.Build());

            var display = new InventoryDisplay(app);

            display.DisplayOverDays();

        }
    }
}
