﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public abstract class CategorizedItem : Item
    {
        public virtual void UpdateQuality() { }
        
        public void UpdateSellIn()
        {
            this.SellIn -= 1;
        }

        public virtual bool IsInBound()
        {
            return this.Quality >= 0 && this.Quality <= 50;
        }
    }
}
