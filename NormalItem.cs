﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public class NormalItem : CategorizedItem
    {
        public readonly int DecreaseQualityValue = 1;
        public override void UpdateQuality()
        {
            this.Quality -= this.DecreaseQualityValue;
            UpdateSellIn();
        }
    }
}
