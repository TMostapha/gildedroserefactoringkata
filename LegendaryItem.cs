﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public class LegendaryItem : CategorizedItem
    {
        public override bool IsInBound()
        {
            return this.Quality == 80;
        }
    }
}
