# Gilded Rose refactoring kata #

For the recruitment process at Wemanity, I'm asked to refactor a little system of inventory management 
to demonstrate my skills in Software Craftsmanship.

### Description ###

The system I'm working on is a system of inventory management. It allows to keep up-to-date the list of items that contains the inventory; 
depends on their quality that decreases the longer they are not sell.

More details of the project can be found [here](https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/main/GildedRoseRequirements.txt).

##### Feature #####

I'm asked to add a new type of items "conjured" that degrades in quality twice as fast as normal items.

##### Programming language #####

I've chosen C# .NET as it's the language I'm mastering the most and the one I'm applying for.

### Refactoring ###

##### Analyses #####

Currently, the logic for the update of items quality is located in one method called "UpdateQuality".
This code is quite too long and contains a lot of conditions. It's an issue for the readability and the maintainability of it.

The solution I found for this is to create types for items.
I've listed 4 basic types plus the new one : 
    * Normal items : their quality decrease by 1 each day
    * Aged items : their quality increase by 1 each day (currently, we only have the "Aged brie", but I assume they'll be more of that kind later. Maybe an "Aged camembert"?)
    * Legendary items : their quality never change in time
    * Unusual items : their logic is too much specific. They need a property that will store their way to compute their quality
    * (NEW) Conjured items : their quality decrease twice as fast as normal items.

Like that, it's easier to add new categories of items and each one is specialized in updating their quality

(Update) :  * My solution is currently working (not counting the unit tests I've to add), but the main program is too heavy of code 
            because of datas building.
            * I plan to separate the build of items in a specific class.
            * I also put the display separetly to "purify" the main method.
            * I think I was gone deep in the problem and it still work to be "perfect", 
            but I don't have the time for and I don't think it's the purpose of this exercice.

##### Unit Tests #####

Before explaining this section, I want to mention that I heard about the Test Driven Development (TDD) method. 
But I did not apply it in this project as it's new for me. But I'm thinking about adopting this way for my future development.

I actually made unit tests on the quality computing algorithm of each type of item and verify if the quality still in the bound between 0 and 50.

To be honest, I did not paid attention to the approval as I never worked on that kind of test and I run out of time to deliver my solution.

### Thanks ###

I would like to thanks you in advance and I hope that my work will please you.