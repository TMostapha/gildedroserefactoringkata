﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public class AgedItem : CategorizedItem
    {
        public override void UpdateQuality()
        {
            this.Quality += 1;
            UpdateSellIn();
        }
    }
}
