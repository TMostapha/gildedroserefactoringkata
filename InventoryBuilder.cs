﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp
{
    public class InventoryBuilder
    {
        private readonly List<CategorizedItem> _items;
        public InventoryBuilder()
        {
            _items = new List<CategorizedItem>
            {
                new NormalItem {Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20},
                new AgedItem {Name = "Aged Brie", SellIn = 2, Quality = 0},
                new NormalItem {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7},
                new LegendaryItem {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80},
                new LegendaryItem {Name = "Sulfuras, Hand of Ragnaros", SellIn = -1, Quality = 80},
				// this conjured item does not work properly yet
				new ConjuredItem {Name = "Conjured Mana Cake", SellIn = 3, Quality = 6},
                UnusualItemSampleBuilder(15, 20),
                UnusualItemSampleBuilder(10, 49),
                UnusualItemSampleBuilder(5, 49)
            };
        }

        public List<CategorizedItem> Build()
        {
            return _items;
        }

        public UnusualItem UnusualItemSampleBuilder(int sellIn, int quality)
        {
            UnusualItem uItem = new UnusualItem { Name = "Backstage passes to a TAFKAL80ETC concert" };
            uItem.QualityEvaluator = () =>
            {
                if (uItem.SellIn <= 10 && uItem.SellIn > 5)
                    uItem.Quality += 2;
                else if (uItem.SellIn <= 5 && uItem.SellIn >= 0)
                    uItem.Quality += 3;
                else if (uItem.SellIn < 0)
                    uItem.Quality = 0;
                else
                    uItem.Quality += 1;
            };
            uItem.SellIn = sellIn;
            uItem.Quality = quality;

            return uItem;
        }
    }
}
